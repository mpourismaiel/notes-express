import app from './index'
import IndexController from './controllers/index'

Object.assign(app, {
  controllers: {
    IndexController
  }
})
