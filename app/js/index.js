import express from 'express'
import routes from './config/router'
import { capitalize } from './helpers/strings'

let app = express()

Object.assign(app, {
  initRoutes () {
    for (let route of Object.keys(routes)) {
      if (route.toLowerCase().indexOf('get ') === 0) {
        let controller = capitalize(routes[route].controller) + 'Controller'
        let fn = this.controllers[controller][routes[route].action]

        this.get(route.substr(4), fn)
      }
    }
  }
})

export default app
