# Notes Express

A very simple app written using expressjs. The main goal is to build a very
simple framework with minimal effort, that is easily configurable, and is also
available in ES6.

There are awesome frameworks available, and I've tried a lot of them. The main
problem of most of them, is the lack of support for ES6.

Right now this app is completely static and only support `get` routes. Also it
needs registering controllers manually. Unfortunately I was unable to find any
modules to import controller modules all together, if I can't find anything
I'll probably write one.

This seems fun!
 
