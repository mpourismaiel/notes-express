const webpack = require('webpack')
const path = require('path')

module.exports = {
  entry: './app/js/app.js',
  output: {
    path: __dirname,
    filename: 'dist/app.js'
  },
  devtool: 'source-map',
  target: 'node',
  resolve: {
    root: path.resolve('./')
  },
  stats: {
    colors: true,
    reasons: true
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel'
      }, {
        test: /\.json$/,
        loader: 'json'
      }
    ]
  }
}
